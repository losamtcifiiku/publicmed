import HtmlPlugin from 'html-webpack-plugin'

const dev = process.env.NODE_ENV !== 'production'

export default {
  mode: dev ? 'development' : 'production',
  entry: './src/app.js',
  devtool: 'source-map',
  output: {
    clean: true,
  },
  module: {
    rules: [
      { test: /\.js$/, exclude: /node_modules/, use: 'babel-loader' },
      { test: /\.svelte$/, use: 'svelte-loader' },
      { test: /\.sass$/, use: ['style-loader', 'css-loader', 'sass-loader'] },
    ],
  },
  plugins: [new HtmlPlugin({ minify: !dev })],
}
