import ws from 'ws'
import express from 'express'
import path from 'path'
import { fileURLToPath } from 'url'

const __dirname = path.dirname(fileURLToPath(import.meta.url))

const wss = new ws.Server({ noServer: true })

const clients = new Map()

function send(sock, val) {
  sock.send(JSON.stringify(val))
}

wss.on('connection', sock => {
  let nick = ''

  sock.on('close', () => {
    clients.delete(nick)
  })

  sock.on('message', res => {
    const data = JSON.parse(res)

    if (data.type === 'handshake') {
      nick = data.value
      clients.set(nick, sock)
      console.log(`handshake: ${nick}`)
      return
    }

    console.log(`${nick} => ${data.target}: ${data.type}`)

    if (!clients.has(data.target)) {
      send(sock, { type: 'error', value: `No such peer: ${data.target}` })
      return
    }

    send(clients.get(data.target), {
      from: nick,
      type: data.type,
      value: data.value,
    })
  })
})

const app = express()

app.use(express.static(path.resolve(__dirname, '..', 'dist')))

const server = app.listen(8080)

server.on('upgrade', (req, sock, head) => {
  wss.handleUpgrade(req, sock, head, sock => {
    wss.emit('connection', sock, req)
  })
})
