export class EventChannelLocal {
  constructor() {
    this.target = new EventTarget()
  }

  on(event, callback) {
    this.target.addEventListener(event, ({ detail }) => callback(detail))
  }

  emit(event, data) {
    this.target.dispatchEvent(new CustomEvent(event, { detail: data }))
  }
}

export default class EventChannel {
  constructor(nick) {
    const url = new URL(location)
    url.protocol = url.protocol.replace('http', 'ws')
    url.pathname = ''
    url.search = ''
    url.hash = ''

    this.handlers = new Map()
    this.sock = new WebSocket(url.href)

    this.sock.onmessage = message => {
      const data = JSON.parse(message.data)
      const callback = this.handlers.get(data.type)
      if (callback) {
        callback(data.from, data.value)
      }
    }

    return new Promise((ok, err) => {
      this.sock.onopen = () => {
        this._send({ type: 'handshake', value: nick })
        ok(this)
      }
      this.sock.onerror = err
    })
  }

  _send(data) {
    this.sock.send(JSON.stringify(data))
  }

  on(event, callback) {
    this.handlers.set(event, callback)
  }

  emit(target, event, value) {
    this._send({ target, type: event, value })
  }
}
